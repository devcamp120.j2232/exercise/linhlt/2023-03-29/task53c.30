package s10;

public class Circle extends Shape{
    protected double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, Boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }
    public double getArea(){
        return Math.PI* this.radius* this.radius;      
    }
    public double getPerimeter(){
        return Math.PI* this.radius * 2;      
    }

    @Override
    public String toString() {
        return "Circle [" + super.toString() + "radius=" + radius + "]";
    }
    
}

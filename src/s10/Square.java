package s10;

public class Square extends Rectangle{
    protected double side;
    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, Boolean filled, double side) {
        super(color, filled, side, side);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    //public void setWidth(double side){
    //};
    //public void setLength(double side){
    //};
    @Override
    public String toString() {
        return "Square [" + super.toString() + "]";
    }
    
    
}
